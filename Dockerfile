FROM node:dubnium-alpine

RUN apk --no-cache add make docker

RUN npm install -g npm && \
    npm install -g @vue/cli && \
    npm cache clean --force

WORKDIR /source